**This project is for demonstration purpose. 
The code is not optimized and well documented.
Many page's functionalities are not fully implemented.**

# How to configure

The configuration properties file located in

`./src/test/resources/selenium.properties`

For now, only chrome is supported.

Then, you need to configure
* Specify location of the WebDriver with the version that matches the chrome version using 
the configuration `browser.driver.chrome.path`. There is a default one is configured,
 you should check if it is match with your chrome version.
* Specify the testing url using the configuration `server.base-url`
* Specify the username using the configuration `server.auth.username`
* Specify the password using the configuration `server.auth.password`


# How to run
After properly configuring, you can run the tests by execute the command 

`gradlew clean test`

# How to view the reports

You can find the html test reports under

`./build/reports/tests`