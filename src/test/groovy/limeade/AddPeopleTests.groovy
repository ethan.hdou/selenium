package limeade

import edou.selenium.SeleniumTest
import limeade.page.AddPeople
import limeade.page.AddPeopleFinish
import limeade.page.Home
import spock.lang.Shared
import spock.lang.Unroll

class AddPeopleTests extends SeleniumTest {

    @Shared
    Home homePage

    @Override
    void setup() {
        super()
        homePage = new Home()
    }

    @Unroll
    void "Add peoples should be successfully - #testcase"() {
        when:
        List<Map> peoples = getPeoples(peopleCount)
        AddPeople addPeoplePage = homePage.goToAddPeople()
        AddPeopleFinish finishPage = addPeoplePage.addPeople(peoples)

        then:
        finishPage.isSucceeded()

        and:
        finishPage.getAddedPeoples() == peoples

        where:
        peopleCount | testcase
        1           | "Single people"
        2           | "Multiple people"
    }

    void "Do nothing when people not provided"() {
        when:
        AddPeople addPeoplePage = homePage.goToAddPeople()
        AddPeopleFinish finishPage = addPeoplePage.addPeople(null)

        then:
        addPeoplePage.isPage()

        and:
        finishPage.isPage() == false
    }

    @Unroll
    void "Do nothing when required data not provided - #testcase"() {
        when:
        AddPeople addPeoplePage = homePage.goToAddPeople()
        AddPeopleFinish finishPage = addPeoplePage.addPeople([people])

        then:
        addPeoplePage.isPage()

        and:
        finishPage.isPage() == false

        where:
        people                                                    | testcase
        [lastName: "lastName", email: "firstNameEmpty@gmail.com"] | "First name empty"
        [firstName: "email Empty", lastName: "lastName"]          | "Email empty"
    }

    protected List<Map> getPeoples(int count) {
        if (count) {
            return (1..count).collect {
                String uuid = UUID.randomUUID().toString().replace("-", "_")
                return [firstName: "FirstName$uuid",
                        lastName : "LastName$uuid",
                        email    : "email.${uuid}@gmail.com"]
            }
        }

        return []
    }
}
