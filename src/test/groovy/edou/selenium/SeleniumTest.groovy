package edou.selenium

import edou.selenium.browser.Browser
import edou.selenium.config.SeleniumConfiguration
import limeade.page.Login
import org.mockito.InjectMocks
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import spock.lang.Shared
import spock.lang.Specification

@ContextConfiguration(classes = SeleniumConfiguration)
@TestPropertySource("classpath:selenium.properties")
class SeleniumTest extends Specification {

    @Autowired
    @InjectMocks
    protected SeleniumConfiguration browserConfiguration

    @Shared
    protected Browser browser

    void setup() {
        browser = Browser.startBrowser(browserConfiguration)
        browser.maximize()
        browser.goToUrl(browserConfiguration.getUrl("auth"))
        Login loginPage = new Login()
        if (loginPage.isPage()) {
            loginPage.login(browserConfiguration.getUsername(), browserConfiguration.getPassword())
        }
    }

    void cleanupSpec() {
        browser?.quit()
        browser = null
    }

    void cleanup() {
        browser?.quit()
    }
}
