package edou.selenium.config

import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.ie.InternetExplorerOptions
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class SeleniumConfiguration {

    static enum BrowserType {
        FIREFOX, CHROME, IE

        @Override
        String toString() {
            return name().toLowerCase()
        }
    }

    @Value('${server.base-url}')
    protected String baseUrl

    @Value('${server.auth.username}')
    protected String username

    @Value('${server.auth.password}')
    protected String password

    // Browser specific
    @Value('${browser.type}')
    protected String browserTypeName
    @Value('${browser.driver.chrome.path}')
    protected String chromeDriverPath
    @Value('${browser.driver.firefox.path}')
    protected String firefoxDriverPath
    @Value('${browser.driver.ie.path}')
    protected String ieDriverPath

    @Value('${browser.options.chrome.headless:false}')
    protected boolean isChromeHeadless

    @Value('${timeout.page-load:1000}')
    protected Long pageLoadTimeout
    @Value('${waiting.stale-element:500}')
    protected Long onStaleElementWaiting
    @Value('${retries.stale-element:20}')
    protected Integer onStaleElementRetries

    String getDriverPath() {
        switch (getBrowserType()) {
            case BrowserType.CHROME:
                return chromeDriverPath
            case BrowserType.FIREFOX:
                return firefoxDriverPath
            case BrowserType.IE:
                return ieDriverPath
        }
    }

    BrowserType getBrowserType() {
        return BrowserType.valueOf(this.browserTypeName.toUpperCase())
    }

    ChromeOptions getChromeOptions() {
        //TODO init chrome options from configuration properties
        ChromeOptions options = new ChromeOptions()
        options.setHeadless(isChromeHeadless)
        if (isChromeHeadless) {
            options.addArguments("--window-size=1920,1080")
            options.addArguments("--start-maximized")
        }
        return options
    }

    FirefoxOptions getFirefoxOptions() {
        //TODO init firefox options from configuration properties
        return new FirefoxOptions()
    }

    InternetExplorerOptions getInternetExplorerOptions() {
        //TODO init firefox options from configuration properties
        return new InternetExplorerOptions()
    }

    Long getPageLoadTimeout() {
        return pageLoadTimeout
    }

    Long getOnStaleElementWaiting() {
        return onStaleElementWaiting
    }

    Integer getOnStaleElementRetries() {
        return onStaleElementRetries
    }

    String getUrl(String target) {
        return getBaseUrl() + (target ?: "")
    }

    String getBaseUrl() {
        return baseUrl
    }

    String getUsername() {
        return username
    }

    String getPassword() {
        return password
    }
}
