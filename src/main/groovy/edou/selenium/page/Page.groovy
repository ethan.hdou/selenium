package edou.selenium.page

import edou.selenium.component.Component

class Page extends Component {
    public static final String XPATH = "//html/body"

    Page() {
        super(XPATH, null)
    }

    void waitForPageLoad(){
        browser.waitForDomReady()
    }


    String getPageTitle() {
        return browser.getDriver().getTitle()
    }

    String getPageUrl() {
        return browser.getCurrentUrl()
    }

    @Override
    protected void initComponent() {}
}
