package edou.selenium.util

class XPathUtil {

    static String concatXPaths(String... xPaths) {
        return xPaths?.findAll()
                     ?.join("")
    }

}
