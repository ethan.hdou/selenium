package edou.selenium.component

import edou.selenium.browser.Browser
import edou.selenium.util.XPathUtil
import org.openqa.selenium.WebElement

abstract class Component {
    protected Component parent
    protected Browser browser
    protected String xPath

    Component(String xPath = null, Component parent = null) {
        this.xPath = xPath
        this.parent = parent
        initBrowser()
        initComponent()
        initXPath()
    }

    String getXPath() { return xPath }

    void click() {
        getElementAndHighlight().click()
    }

    void clickAndWait(long timeoutInMilliseconds) {
        click()
        sleep(timeoutInMilliseconds)
    }

    boolean isPresent() {
        try {
            highlight()
            return true
        } catch (IllegalStateException ex) {
            return false
        }
    }

    String getAttribute(String name) {
        return getElementAndHighlight().getAttribute(name)
    }

    String getText() {
        return getElementAndHighlight().getText()
    }

    protected void initBrowser() {
        browser = Browser.getBrowser()
        if (!browser) {
            throw new IllegalStateException("A com.ethan.selenium.browser is not started")
        }
    }

    protected void initXPath() {
        xPath = XPathUtil.concatXPaths(parent?.getXPath(), xPath)
    }

    protected WebElement getElementAndHighlight() {
        highlight()
        return getElement()
    }

    protected WebElement getElement() {
        return browser.findElementByXPath(this.xPath)
    }

    protected highlight() {
        browser.highlight(xPath)
    }

    abstract protected void initComponent()
}
