package edou.selenium.component

class TextBox extends Component {
    TextBox(String xPath, Component parent = null) {
        super(xPath, parent)
    }

    void type(String text) {
        highlight()
        if (text) {
            getElement().sendKeys(text as CharSequence)
        }
    }

    void typeAndWait(String text, long timeoutInMilliseconds) {
        type(text)
        sleep(timeoutInMilliseconds)
    }

    @Override
    protected void initComponent() {

    }
}
