package edou.selenium.browser

import edou.selenium.config.SeleniumConfiguration
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver

import java.nio.file.Paths

class Chrome extends Browser {
    private static final String CHROME_DRIVER_SYS_PROPERTY_NAME = "webdriver.chrome.driver"

    protected Chrome(SeleniumConfiguration configuration) {
        super(configuration)
    }

    protected WebDriver loadDriver() {
        System.setProperty(CHROME_DRIVER_SYS_PROPERTY_NAME, Paths.get(configuration.getDriverPath()).toString())
        return new ChromeDriver(configuration.getChromeOptions())
    }

    static Browser startBrowser(SeleniumConfiguration configuration) {
        return new Chrome(configuration)
    }

}
