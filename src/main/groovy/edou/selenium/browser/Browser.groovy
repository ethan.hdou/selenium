package edou.selenium.browser

import edou.selenium.config.SeleniumConfiguration
import edou.selenium.config.SeleniumConfiguration.BrowserType
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait

import java.time.Duration
import java.util.concurrent.TimeoutException

/**
 * Browser for testing, handle com.ethan.selenium.browser actions: open, quit, navigation,...
 * Implementation should handle driver loading
 * */
abstract class Browser {
    private static final String HIGHLIGHT_STYLE = "background-color: yellow;box-shadow:0 0 3px 3px yellow;-ms-box-shadow:0 0 3px 3px yellow;";

    protected static WebDriver driver
    protected static SeleniumConfiguration configuration
    protected static Browser browser
    protected static JavascriptExecutor jsExecutor

    protected Browser(SeleniumConfiguration configuration) {
        this.configuration = configuration
        driver = loadDriver()
        jsExecutor = driver as JavascriptExecutor
    }

    static Browser startBrowser(SeleniumConfiguration configuration) {
        switch (configuration.getBrowserType()) {
            case BrowserType.CHROME:
                browser = Chrome.startBrowser(configuration)
                break
        }

        return browser
    }

    static Browser getBrowser() {
        return browser
    }

    static WebDriver getDriver() {
        return driver
    }

    static SeleniumConfiguration getConfiguration() {
        return configuration
    }

    String getCurrentUrl() {
        return driver.getCurrentUrl()
    }

    void goToUrl(String url) {
        driver.navigate().to(url)
        waitForDomReady()
    }

    void goBack() {
        driver.navigate().back()
        waitForDomReady()
    }

    void goForward() {
        driver.navigate().forward()
        waitForDomReady()
    }

    void reloadPage() {
        driver.navigate().refresh()
        waitForDomReady()
    }

    void maximize() {
        driver.manage().window().maximize()
    }

    void quit() {
        driver.quit()
    }

    void highlight(String xpath) {
        String expression = "arguments[0].setAttribute('style', arguments[1]);"
        WebElement webElement = findElementByXPath(xpath)
        String currentStyle = webElement.getAttribute("style")
        runScript(expression, webElement, HIGHLIGHT_STYLE + (currentStyle ?: ""))
        sleep(200)
        if (currentStyle != null) {
            runScript(expression, webElement, currentStyle)
        }
    }


    List<WebElement> findElementsByXPath(String xPath) {
        int currentRetries = 0
        List<WebElement> elementsFound = driver.findElements(By.xpath(xPath));
        while (elementsFound.isEmpty() && driver.findElements(By.xpath("//html")).isEmpty() && currentRetries <= configuration.getOnStaleElementRetries()) {
            currentRetries++
            sleep(configuration.getOnStaleElementWaiting())
            elementsFound = driver.findElements(By.xpath(xPath))
        }
        return elementsFound
    }

    WebElement findElementByXPath(String xPath) throws IllegalStateException {
        if (xPath == null) {
            throw new IllegalStateException("xPath is null")
        }
        List<WebElement> elements = findElementsByXPath(xPath)
        int elementCount = elements.size()
        if (elementCount == 0) {
            throw new IllegalStateException(String.format("No elements found with xPath: %s", xPath))
        }
        if (elementCount > 1) {
            throw new IllegalStateException(String.format("%s elements found with xPath: %s", elementCount, xPath))
        }
        return elements.first()
    }

    void waitForDomReady() throws TimeoutException {
        waitForCondition(configuration.getPageLoadTimeout(), "Timeout waiting for page load!") { getDOMReadyState() == "complete" }
    }

    void waitForCondition(long timeoutInMilliseconds, String timeoutMessage, Closure<Boolean> validator) {
        ExpectedCondition<Boolean> condition = getExpectedCondition(validator)
        WebDriverWait wait = waitForMilliseconds(timeoutInMilliseconds)
        int staleElementTries = 0
        while (true) {
            try {
                wait.until(condition)
                break
            } catch (StaleElementReferenceException ex) {
                if (staleElementTries < configuration.getOnStaleElementRetries()) {
                    staleElementTries++
                    continue
                }
                throw new TimeoutException(timeoutMessage, ex)
            }
        }
    }

    String getDOMReadyState() {
        return getEval("document.readyState") as String
    }

    void runScript(String expression, Object... params) {
        List scriptParams = params ?: []
        jsExecutor.executeScript(expression, *scriptParams)
    }

    Object getEval(String expr, Object... params) {
        String exprWithReturn = createExpressionWithReturn(expr)
        try {
            return jsExecutor.executeScript(exprWithReturn, params)
        } catch (Exception ex) {
            throw new IllegalStateException("Failed to execute: " + exprWithReturn, ex)
        }
    }

    protected String createExpressionWithReturn(String expr) {
        return "return (" + expr + ")"
    }

    protected ExpectedCondition<Boolean> getExpectedCondition(Closure<Boolean> validator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            Boolean apply(WebDriver webDriver) {
                return validator(webDriver)
            }
        }
    }

    protected WebDriverWait waitForMilliseconds(Long timeoutInMilliseconds) {
        return new WebDriverWait(driver, Duration.ofMillis(timeoutInMilliseconds))
    }

    abstract protected WebDriver loadDriver()

}