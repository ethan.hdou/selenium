package limeade.component

import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class SiteBar extends Component {
    protected SiteBarItemGroup itemSettings
    protected SiteBarItemLink itemAddPeople

    SiteBar(String xpath = null, Component parent = null) {
        super(xpath, parent)
        itemSettings = new SiteBarItemGroup("Settings", this)
        itemAddPeople = new SiteBarItemLink("Add People", this)
    }

    void clickSettings(Long timeoutInMilliseconds = null) {
        timeoutInMilliseconds ? itemSettings.clickAndWait(timeoutInMilliseconds) : itemSettings.click()
    }

    void clickAddPeople(Long timeoutInMilliseconds = null) {
        timeoutInMilliseconds ? itemAddPeople.clickAndWait(timeoutInMilliseconds) : itemAddPeople.click()
    }

    @Override
    protected void initComponent() {

    }

    class SiteBarItemGroup extends Component {

        SiteBarItemGroup(String label = null, Component parent = null) {
            super("${XPath.fromTemplate(Template.SITE_BAR_ITEM_SELECTOR, label)}${Template.SITE_BAR_ITEM_GROUP_SELECTOR.getValue()}", parent)
        }

        @Override
        protected void initComponent() {

        }
    }

    class SiteBarItemLink extends Component {

        SiteBarItemLink(String label = null, Component parent = null) {
            super("${XPath.fromTemplate(Template.SITE_BAR_ITEM_SELECTOR, label)}${Template.SITE_BAR_ITEM_LINK_SELECTOR.getValue()}", parent)
        }

        @Override
        protected void initComponent() {

        }
    }
}
