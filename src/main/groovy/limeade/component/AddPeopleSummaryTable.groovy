package limeade.component

import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class AddPeopleSummaryTable extends HTMLTable {

    AddPeopleSummaryTable(Integer index, Component parent = null) {
        super(index, parent)
    }

    List<Map> getSummary() {
        int rowCount = getRowCount()
        if (rowCount) {
            return (1..rowCount).collect { Integer rowIndex ->
                SummaryRow summaryRow = new SummaryRow(rowIndex, this)

                return [firstName: summaryRow.getFirstName(),
                        lastName : summaryRow.getLastName(),
                        email    : summaryRow.getEmail()]
            }
        }

        return []
    }

    @Override
    protected void initComponent() {

    }

    class SummaryRow extends Component {

        protected static final String dataOriginalAttribute = "data-original-title"
        protected static final String dataOriginalXpath = "//div[@$dataOriginalAttribute]"
        protected static final String firstNameXpath = XPath.fromTemplate(Template.HTML_TABLE_COLUMN_SELECTOR, "1") + dataOriginalXpath
        protected static final String lastNameXpath = XPath.fromTemplate(Template.HTML_TABLE_COLUMN_SELECTOR, "2") + dataOriginalXpath
        protected static final String emailXpath = XPath.fromTemplate(Template.HTML_TABLE_COLUMN_SELECTOR, "3") + dataOriginalXpath
        protected GenericComponent firstName
        protected GenericComponent lastName
        protected GenericComponent email

        SummaryRow(Integer index, Component table) {
            super(XPath.fromTemplate(Template.HTML_TABLE_ROW_SELECTOR, index.toString()), table)
            firstName = new GenericComponent(firstNameXpath, this)
            lastName = new GenericComponent(lastNameXpath, this)
            email = new GenericComponent(emailXpath, this)
        }

        String getFirstName() {
            return this.firstName.getAttribute(dataOriginalAttribute)
        }

        String getLastName() {
            return this.lastName.getAttribute(dataOriginalAttribute)
        }

        String getEmail() {
            return this.email.getAttribute(dataOriginalAttribute)
        }

        @Override
        protected void initComponent() {

        }
    }
}
