package limeade.component

import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class ErrorStatus extends Component {

    ErrorStatus(Component parent = null) {
        super(XPath.fromTemplate(Template.ERROR_STATUS_SELECTOR), parent)
    }

    @Override
    protected void initComponent() {

    }
}
