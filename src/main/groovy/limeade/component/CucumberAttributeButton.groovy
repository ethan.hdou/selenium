package limeade.component

import edou.selenium.component.Button
import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class CucumberAttributeButton extends Button {
    CucumberAttributeButton(String identifier, Component parent = null) {
        super(XPath.fromTemplate(Template.BUTTON_DIV_CUCUMBER_ATTRIBUTE_SELECTOR, identifier), parent)
    }
}
