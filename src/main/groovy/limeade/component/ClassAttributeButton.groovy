package limeade.component

import edou.selenium.component.Button
import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class ClassAttributeButton extends Button {
    ClassAttributeButton(String className, Component parent = null) {
        super(XPath.fromTemplate(Template.BUTTON_DIV_CLASS_ATTRIBUTE_SELECTOR, className), parent)
    }
}
