package limeade.component

import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class HTMLTable extends Component {

    protected static final String TABLE_BODY = "/tbody"
    protected String rowXpath

    HTMLTable(Integer index, Component parent = null) {
        super(XPath.fromTemplate(Template.HTML_TABLE_SELECTOR, index.toString()), parent)
        rowXpath = this.getXPath() + TABLE_BODY + XPath.fromTemplate(Template.HTML_TABLE_ROWS_SELECTOR)
    }

    int getRowCount() {
        return browser.findElementsByXPath(rowXpath).size()
    }

    @Override
    protected void initComponent() {

    }
}
