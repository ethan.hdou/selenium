package limeade.component

import edou.selenium.component.Component

class GenericComponent extends Component {
    GenericComponent(String xpath, Component parent) {
        super(xpath, parent)
    }

    @Override
    protected void initComponent() {

    }
}
