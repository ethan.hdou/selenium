package limeade.component

import edou.selenium.component.Component
import edou.selenium.component.TextBox
import limeade.util.XPath
import limeade.util.XPath.Template

class CucumberAttributeInput extends TextBox {
    CucumberAttributeInput(String identifier, Component parent = null) {
        super(XPath.fromTemplate(Template.TEXT_BOX_CUCUMBER_ATTRIBUTE_SELECTOR, identifier), parent)
    }

}
