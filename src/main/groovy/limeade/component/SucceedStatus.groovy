package limeade.component

import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class SucceedStatus extends Component {

    SucceedStatus(Component parent = null) {
        super(XPath.fromTemplate(Template.SUCCEED_STATUS_SELECTOR), parent)
    }

    @Override
    protected void initComponent() {

    }
}
