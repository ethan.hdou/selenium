package limeade.component

import edou.selenium.component.Component
import edou.selenium.component.TextBox
import limeade.util.XPath
import limeade.util.XPath.Template

class FieldAttributeInput extends TextBox {
    FieldAttributeInput(String identifier, Component parent = null) {
        super(XPath.fromTemplate(Template.TEXT_BOX_FIELD_ATTRIBUTE_SELECTOR, identifier), parent)
    }

}
