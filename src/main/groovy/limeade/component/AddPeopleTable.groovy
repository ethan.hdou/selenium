package limeade.component

import edou.selenium.component.Component
import limeade.util.XPath
import limeade.util.XPath.Template

class AddPeopleTable extends Component {

    AddPeopleTable(Integer index, Component parent = null) {
        super(XPath.fromTemplate(Template.HTML_TABLE_SELECTOR, index.toString()), parent)
    }

    void fillRows(List<Map> rows) {
        rows?.eachWithIndex { Map row, int index -> fillRow(row, index + 1) }
    }

    void fillRow(Map row, Integer index = null) {
        AddPeopleRow rowElement = new AddPeopleRow(index, this)
        rowElement.fillFirstName(row.firstName)
        rowElement.fillLastName(row.lastName)
        rowElement.fillEmail(row.email)
    }


    @Override
    protected void initComponent() {

    }

    class AddPeopleRow extends Component {

        protected FieldAttributeInput firstName
        protected FieldAttributeInput lastName
        protected FieldAttributeInput email

        AddPeopleRow(Integer index, Component table) {
            super(XPath.fromTemplate(Template.HTML_TABLE_ROW_SELECTOR, index.toString()), table)
            firstName = new FieldAttributeInput("firstName", this)
            lastName = new FieldAttributeInput("lastName", this)
            email = new FieldAttributeInput("email", this)
        }

        void fillFirstName(String firstName) {
            this.firstName.typeAndWait(firstName, 1000)
        }

        void fillLastName(String lastName) {
            this.lastName.typeAndWait(lastName, 1000)
        }

        void fillEmail(String email) {
            this.email.typeAndWait(email, 1000)
        }

        void fillStartDate(Date date) {

        }

        void fillManager(String manager) {

        }

        void fillSegment(String segment) {

        }

        void fillTitle(String title) {

        }

        @Override
        protected void initComponent() {

        }
    }
}
