package limeade.util

class XPath {
    static enum Template {
        TEXT_BOX_CUCUMBER_ATTRIBUTE_SELECTOR("//input[@data-cucumber='%s']"),
        TEXT_BOX_FIELD_ATTRIBUTE_SELECTOR("//input[@field='%s']"),
        BUTTON_DIV_CUCUMBER_ATTRIBUTE_SELECTOR("//div[@data-cucumber='%s']"),
        BUTTON_DIV_CLASS_ATTRIBUTE_SELECTOR("//div[contains(@class,'%s')]"),
        SITE_BAR_ITEM_SELECTOR("//div[@data-cucumber='sitebar-item-trigger' and .//span[@data-cucumber='menu-item-label' and text()='%s']]"),
        SITE_BAR_ITEM_GROUP_SELECTOR("/div[1]"),
        SITE_BAR_ITEM_LINK_SELECTOR("/a[1]"),
        HTML_TABLE_SELECTOR("//table[%s]"),
        HTML_TABLE_ROWS_SELECTOR("//tr"),
        HTML_TABLE_ROW_SELECTOR("//tr[%s]"),
        HTML_TABLE_COLUMNS_SELECTOR("/td"),
        HTML_TABLE_COLUMN_SELECTOR("/td[%s]"),
        SUCCEED_STATUS_SELECTOR("//i[text()='check_circle']/parent::*"),
        ERROR_STATUS_SELECTOR("//i[text()='error']/parent::*"),
        DIV_DATA_ORIGINAL_TITLE_ATTRIBUTE_SELECTOR("//div[@data-original-title]")

        private String value

        Template(String value) {
            this.value = value
        }

        String getValue() {
            return value
        }
    }

    static String fromTemplate(String template, String... identifiers) {
        return fromTemplate(Template.valueOf(template.toUpperCase()), *identifiers)
    }

    static String fromTemplate(Template template, String... identifiers) {
        return String.format(template.getValue(), *identifiers)
    }
}
