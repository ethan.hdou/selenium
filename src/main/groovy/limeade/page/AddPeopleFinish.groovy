package limeade.page

import edou.selenium.page.Page
import limeade.component.AddPeopleSummaryTable
import limeade.component.ErrorStatus
import limeade.component.SucceedStatus

class AddPeopleFinish extends Page {

    protected static final String SLUG = "/home/tps/invite/finish"
    protected SucceedStatus successStatus
    protected ErrorStatus errorStatus
    protected AddPeopleSummaryTable summaryTable

    AddPeopleFinish() {
        successStatus = new SucceedStatus(this)
        errorStatus = new ErrorStatus(this)
        summaryTable = new AddPeopleSummaryTable(1, this)
    }

    boolean isSucceeded() {
        assertPage()
        successStatus.isPresent()
    }

    String getSuccessMessage() {
        successStatus.getText()
    }

    boolean isFailed() {
        assertPage()
        errorStatus.isPresent()
    }

    String getFailureMessage() {
        errorStatus.getText()
    }

    List<Map> getAddedPeoples() {
        assertPage()
        if (isSucceeded()) {
            return summaryTable.getSummary()
        }

        return []
    }


    boolean isPage() {
        return getPageUrl().endsWith(SLUG)
    }

    void assertPage() {
        assert isPage(): "Current page is not 'Add People Finiush' page"
    }
}
