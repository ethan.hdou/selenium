package limeade.page


import edou.selenium.page.Page
import limeade.component.SiteBar

class Home extends Page {
    protected SiteBar siteBar

    Home() {
        siteBar = new SiteBar(null, this)
    }

    AddPeople goToAddPeople() {
        sleep(5000)
        siteBar.clickSettings(1000)
        siteBar.clickAddPeople(1000)
        sleep(2000)
        return new AddPeople()
    }
}
