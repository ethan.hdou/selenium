package limeade.page

import edou.selenium.component.Button
import edou.selenium.component.TextBox
import edou.selenium.page.Page
import limeade.component.CucumberAttributeButton
import limeade.component.CucumberAttributeInput

class Login extends Page {
    private static final String TITLE = "TINYpulse | Login"
    protected TextBox username
    protected TextBox password
    protected Button continueButton
    protected Button signInButton

    Login() {
        username = new CucumberAttributeInput("input-login-email", this)
        password = new CucumberAttributeInput("input-login-password", this)
        continueButton = new CucumberAttributeButton("button-continue", this)
        signInButton = new CucumberAttributeButton("button-login", this)
    }

    TextBox getUsername() {
        return username
    }

    TextBox getPassword() {
        return password
    }

    Button getContinue() {
        return continueButton
    }

    Button getSignIn() {
        return signInButton
    }

    void login(String username, String password) {
        assertPage()
        getUsername().typeAndWait(username, 1000)
        getContinue().clickAndWait(3000)
        getPassword().typeAndWait(password, 1000)
        getSignIn().clickAndWait(3000)
        waitForPageLoad()
    }

    boolean isPage() {
        return getPageTitle() == TITLE
    }

    void assertPage() {
        assert isPage(): "Current page is not 'Login' page"
    }
}
