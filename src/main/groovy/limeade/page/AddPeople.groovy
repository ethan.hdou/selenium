package limeade.page

import edou.selenium.page.Page
import limeade.component.AddPeopleTable
import limeade.component.ClassAttributeButton

class AddPeople extends Page {
    private static final String TITLE = "TINYpulse | Add People"
    protected AddPeopleTable addPeopleTable
    protected ClassAttributeButton addPeopleButton

    AddPeople() {
        addPeopleTable = new AddPeopleTable(1, this)
        addPeopleButton = new ClassAttributeButton("cucumber-send-invite-button", this)
    }

    AddPeopleFinish addPeople(List<Map> peoples) {
        assertPage()
        addPeopleTable.fillRows(peoples)
        addPeopleButton.clickAndWait(2000)

        return new AddPeopleFinish()
    }

    boolean isPage() {
        return getPageTitle() == TITLE
    }

    void assertPage() {
        assert isPage(): "Current page is not 'Add People' page"
    }
}
